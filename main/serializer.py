from rest_framework import serializers, filters
from .models import *


class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = ['id', 'key', 'title', 'role']


class UserSerializer(serializers.ModelSerializer):
    permissions = PermissionSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'phone', 'password', 'role', 'permissions', 'created_at', 'avatar']



class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ['id', 'latitude', 'longitude', 'label']
class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'name']


class ColorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Color
        fields = ['id', 'name']

class ProductSerializer(serializers.ModelSerializer):
    # category = CategorySerializer()
    # color = ColorSerializer()

    class Meta:
        model = Product
        fields = ['id', 'name', 'category', 'body_price', 'selling_price', 'min_debt_price', 'min_cash_price', 'amount',
                  'color', 'description']




class WarehouseSerializer(serializers.ModelSerializer):
    # location = LocationSerializer()

    class Meta:
        model = Warehouse
        fields = ['id', 'name', 'address', 'location', 'phone']


class PartySerializer(serializers.ModelSerializer):
    user = UserSerializer()
    warehouse = WarehouseSerializer()

    class Meta:
        model = Party
        fields = ['id', 'user', 'warehouse']


class GroupSerializer(serializers.ModelSerializer):
    # product = ProductSerializer()
    # party = PartySerializer()

    class Meta:
        model = Group
        fields = ['id', 'product', 'party']


class InvoiceSerializer(serializers.ModelSerializer):
    # party = PartySerializer()

    class Meta:
        model = Invoice
        fields = ['id', 'name', 'type', 'comment', 'party']


class CourierSerializer(serializers.ModelSerializer):
    # user = UserSerializer()

    class Meta:
        model = Courier
        fields = ['id', 'user', 'car_number']


class OperatorSerializer(serializers.ModelSerializer):
    # user = UserSerializer()

    class Meta:
        model = Operator
        fields = ['id', 'user', 'date']


class AccountantSerializer(serializers.ModelSerializer):
    # user = UserSerializer()

    class Meta:
        model = Accountant
        fields = ['id', 'user']


class StorekeeperSerializer(serializers.ModelSerializer):
    # user = UserSerializer()

    class Meta:
        model = Storekeeper
        fields = ['id', 'user']


class WorkPlaceSerializer(serializers.ModelSerializer):
    # location = LocationSerializer()

    class Meta:
        model = WorkPlace
        fields = ['id', 'name', 'address', 'location']

class ClientSerializer(serializers.ModelSerializer):
    # location = LocationSerializer()
    # workplace = WorkPlaceSerializer()

    class Meta:
        model = Client
        fields = ['id', 'first_name', 'last_name', 'phone', 'extra_phone', 'address', 'location', 'passport_number',
                  'passport_picture', 'workplace']
        filter_backends = [filters.SearchFilter]
        search_fields = ['first_name', 'last_name', 'phone']






class OrderSerializer(serializers.ModelSerializer):
    # client = ClientSerializer()
    # product = ProductSerializer()

    class Meta:
        model = Order
        fields = ['id', 'client', 'product', 'count', 'cost', 'payment_type', 'is_debt', 'month', 'advance',
                  'monthly_payment_date', 'monthly_payment_amount']


class MonthlyPaymentSerializer(serializers.ModelSerializer):
    # client = ClientSerializer()

    class Meta:
        model = MonthlyPayment
        fields = ['id', 'client', 'amount', 'payment_method', 'receive_date', 'screenshot']
