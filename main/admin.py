from django.contrib import admin

from main.models import *

admin.site.register(User)
admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Color)
admin.site.register(Warehouse)
admin.site.register(Party)
admin.site.register(Group)
admin.site.register(Invoice)
admin.site.register(Courier)
admin.site.register(Operator)
admin.site.register(Accountant)
admin.site.register(Storekeeper)
admin.site.register(Client)
admin.site.register(WorkPlace)
admin.site.register(Location)
admin.site.register(Order)
admin.site.register(MonthlyPayment)
