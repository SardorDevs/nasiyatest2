from django.urls import path, include
from rest_framework.routers import DefaultRouter
from main.views.views import *

router = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'products', ProductViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'colors', ColorViewSet)
router.register(r'warehouses', WarehouseViewSet)
router.register(r'parties', PartyViewSet)
router.register(r'groups', GroupViewSet)
router.register(r'invoices', InvoiceViewSet)
router.register(r'couriers', CourierViewSet)
router.register(r'operators', OperatorViewSet)
router.register(r'accountants', AccountantViewSet)
router.register(r'storekeepers', StorekeeperViewSet)
router.register(r'clients', ClientViewSet)
router.register(r'workplaces', WorkPlaceViewSet)
router.register(r'locations', LocationViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'order-history/(?P<client_id>\d+)', OrderHistoryViewSet, basename='order-history')
router.register(r'monthlypayments', MonthlyPaymentViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
