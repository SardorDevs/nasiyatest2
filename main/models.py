from django.db import models

from django.db import models
from jsonschema.exceptions import ValidationError


class User(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    phone = models.IntegerField()
    password = models.CharField(max_length=255)
    role = models.CharField(choices=(('admin', 'Admin'), ('accountant', 'Accountant'), ('storekeeper', 'Storekeeper'), ('courier', 'Courier'), ('operator', 'Operator')), max_length=255)
    permissions = models.ManyToManyField('Permission')
    created_at = models.DateTimeField(auto_now_add=True)
    avatar = models.ImageField(upload_to='avatars/', null=True, blank=True)

class Category(models.Model):
    name = models.CharField(max_length=255)

class Color(models.Model):
    name = models.CharField(max_length=255)
class Product(models.Model):
    name = models.CharField(max_length=255)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    body_price = models.DecimalField(max_digits=10, decimal_places=2)
    selling_price = models.DecimalField(max_digits=10, decimal_places=2)
    min_debt_price = models.DecimalField(max_digits=10, decimal_places=2)
    min_cash_price = models.DecimalField(max_digits=10, decimal_places=2)
    amount = models.IntegerField()
    color = models.ForeignKey(Color, on_delete=models.CASCADE)
    description = models.TextField()



class Warehouse(models.Model):
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    location = models.ForeignKey('Location', on_delete=models.CASCADE)
    phone = models.CharField(max_length=20)

class Party(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    warehouse = models.ForeignKey(Warehouse, on_delete=models.CASCADE)

class Group(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    party = models.ForeignKey(Party, on_delete=models.CASCADE)

class Invoice(models.Model):
    name = models.CharField(max_length=255)
    type = models.CharField(choices=(('monthly', 'Monthly'), ('one_time', 'One Time'), ('lost', 'Lost')), max_length=255)
    comment = models.TextField()
    party = models.ForeignKey(Party, on_delete=models.CASCADE)

class WorkPlace(models.Model):
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    location = models.ForeignKey('Location', on_delete=models.CASCADE)

class Location(models.Model):
    latitude = models.BigIntegerField()
    longitude = models.BigIntegerField()
    label = models.CharField(max_length=255)

class Courier(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    car_number = models.CharField(max_length=255)



class CourierHistory(models.Model):
    courier_id = models.ForeignKey(
        Courier,
        on_delete=models.CASCADE,
        related_name='courier_histories'
    )
    group_id = models.ForeignKey(Group, on_delete=models.CASCADE)
    get_products = models.IntegerField()
    return_products = models.IntegerField(null=True, blank=True)
    broken_products = models.IntegerField(null=True, blank=True)
    date = models.DateField(auto_now=True, null=True, blank=True)

    def __str__(self) -> str:
        return f"{self.courier_id}, {self.group_id}"

    class Meta:
        verbose_name = 'CourierHistory'
        verbose_name_plural = 'CourierHistories'
        db_table = 'courier_history'



class CourierProduct(models.Model):
    group_id = models.ForeignKey(
        to=Group,
        on_delete=models.CASCADE,
        related_name='courier_groups'
    )
    count = models.IntegerField()
    courier_id = models.ForeignKey(
        to=Courier,
        on_delete=models.CASCADE,
        related_name='courier_products'
    )

    def __str__(self):
        return f"Courier - {self.courier_id}, group_id - {self.group_id}"

    class Meta:
        verbose_name = 'CourierProduct'
        verbose_name_plural = 'CourierProducts'
        db_table = 'courier_product'




class Operator(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField()

class Accountant(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class Permission(models.Model):
    key = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    role = models.CharField(choices=(('admin', 'Admin'), ('accountant', 'Accountant'), ('storekeeper', 'Storekeeper'), ('courier', 'Courier'), ('operator', 'Operator')), max_length=255)

class Storekeeper(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class Client(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    phone = models.CharField(max_length=20)
    extra_phone = models.CharField(max_length=20)
    address = models.CharField(max_length=255)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    passport_number = models.CharField(max_length=255)
    passport_picture = models.ImageField(upload_to='passport_pictures/', null=True, blank=True)
    workplace = models.ForeignKey(WorkPlace, on_delete=models.CASCADE, null=True, blank=True)



def validate_month(value):
    if value < 1 or value > 8:
        raise ValidationError(('Month must be between 1 and 8.'),
            params={'value': value},
        )

class Order(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    count = models.IntegerField()
    cost = models.DecimalField(max_digits=10, decimal_places=2)
    payment_type = models.CharField(choices=(('debt', 'Debt'), ('cash', 'Cash')), max_length=255)
    is_debt = models.BooleanField()
    month = models.IntegerField(validators=[validate_month] , null=True, blank=True)
    advance = models.DecimalField(max_digits=10, decimal_places=2)
    monthly_payment_date = models.DateTimeField()
    monthly_payment_amount = models.DecimalField(max_digits=10, decimal_places=2)

    def save(self, *args, **kwargs):
        self.cost = self.product.price * self.count
        super().save(*args, **kwargs)

class MonthlyPayment(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    payment_method = models.CharField(choices=(('cash', 'Cash'), ('card', 'Card')), max_length=255)
    receive_date = models.DateTimeField()
    screenshot = models.ImageField(upload_to='payment_screenshots/')
